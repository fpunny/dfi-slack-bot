import React from 'react';
import { Mixins } from '@cheapreats/react-ui';
import styled from 'styled-components';

export const Container = ({
    parentAs,
    contentAs,
    contentClass,
    children,
    ...props
}) => (
    <Wrapper { ...props } as={ parentAs }>
        <Content as={ contentAs } className={ contentClass }>
            { children }
        </Content>
    </Wrapper>
);

const Wrapper = styled.div`
    ${ Mixins.flex('column', 'flex-start', 'center') }
    box-sizing: border-box;
    width: 100%;
`;

const Content = styled.div`
    background-color: white;
    box-sizing: border-box;
    padding: 20px 30px;
    max-width: 1000px;
    min-height: 100vh;
    width: 100%;
`;