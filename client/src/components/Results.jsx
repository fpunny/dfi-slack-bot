import React, { Fragment, useState, useEffect } from 'react';
import { Sync } from 'styled-icons/fa-solid';
import styled from 'styled-components';
import moment from 'moment';

import { Heading, SmallText, Paragraph, Button as B, Mixins } from '@cheapreats/react-ui';
import { useFetch } from '../utils/hooks';
const DEBOUNCE = 500;

const renderQuestions = items => (
    items.map(({ _id, text, created_at, answer, answered_at }) => (
        <Item key={ _id } answered={answer}>
            <Heading type='h3' bold size='1.3rem'>{ text }</Heading>
            {
                answer ?
                <Paragraph margin='10px 0 0'>{ answer }</Paragraph> :
                <SmallText type='p' bold>No Answer :c</SmallText>
            }
            <Dates>
                <SmallText bold>Created at: { moment(created_at).format('LLLL') }</SmallText>
                <SmallText bold>Answered at: {
                    answer ? moment(answered_at).format('LLLL') : 'Find out next time in Dragonball Z'
                }</SmallText>
            </Dates>
        </Item>
    ))
);

export const Results = ({ search }) => {
    const [ _search, setSearch ] = useState(search);
    const [ result, refresh ] = useFetch(_search);
    useEffect(() => {
        const timer = window.setTimeout(() => {
            setSearch(`${ process.env.REACT_APP_API }?search=${ search }`);
        }, [ DEBOUNCE ]);
        return () => window.clearTimeout(timer);
    }, [ search ]);

    return (
        <Fragment>
            <Heading type='h2' bold margin='0'>Results</Heading>
            <Button
                icon={ Sync }
                onClick={ refresh }
                loading={ !result }
            >Refresh</Button>
            {
                result ? (
                    <List>
                        {
                            result.length ? (
                                renderQuestions(result)
                            ) : <Item empty>No Questions</Item>
                        }
                    </List>
                ) : <Loading bold margin='-10px 0 0'>Loading...</Loading>
            }
        </Fragment>
    )
};

const Button = styled(B)`
    margin-bottom: 10px;
    padding: 8px 26px;
`;

const List = styled.ul`
    list-style-type: none;
    margin-top: 30px;
    padding: 0;
`;

const Item = styled.li`
    ${ Mixins.flex('column') }
    box-sizing: border-box;
    margin-bottom: 15px;
    padding: 0 20px;
    border-left: 6px solid ${
        ({theme, answered}) => answered ? (
            '#0066ff'
        ) : theme.colors.primary
    };
    ${({empty}) => empty ? `
        border: none;
        font-weight: bold;
    ` : ''}
`;

const Dates = styled.div`
    ${ Mixins.flex('column') }
    text-align: right;
    margin-left: auto;
`;

const Loading = styled(SmallText)`
    animation: loading 1s ease-in-out 0s infinite;
    @keyframes loading {
        0% { opacity: 1 }
        50% { opacity: 0.6 }
        100% { opacity: 1 }
    }
`;