/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useCallback } from 'react';

export const useFetch = (
    url = process.env.REACT_APP_API,
    options = {},
) => {
    const [data, setData] = useState();
    const [refresh, setRefresh] = useState(false);
    const refetch = useCallback(() => setRefresh(s => !s), []);

    useEffect(() => {
        let mounted = true;
        if (data) setData();

        (async () => {
            try {
                const res = await fetch(url, {
                    headers: { 'Content-Type': 'application/json' },
                    ...options
                });
                const d = await res.json();
                if (mounted) setData(d);
            } catch (e) {
                console.error(e);
            }
        })();
        return () => mounted = false;
    }, [ refresh, url ]);

    return [ data, refetch ];
}