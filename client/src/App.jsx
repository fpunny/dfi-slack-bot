import React from 'react';
import { Switch, Route } from 'react-router-dom';
import * as pages from './pages';

const App = () => (
    <Switch>
        {
            Object.values(pages).map(page => (
                <Route
                    { ...page.routerProps }
                    component={ page }
                    key={ page.name }
                />
            ))
        }
    </Switch>
);

export default App;
