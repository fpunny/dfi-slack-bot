import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Bitbucket } from 'styled-icons/fa-brands';
import { SmallText } from '@cheapreats/react-ui';
import { Container } from '../components';

export default ({ children, ...props }) => (
    <Fragment>
        <Container contentAs='main' { ...props }>
            { children }
            <Footer>
                <SmallText margin='0' bold>
                    <Icon/>
                    <a 
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://bitbucket.org/fpunny/dfi-slack-bot/src/master/'
                    >
                        Bitbucket Repo
                    </a>
                </SmallText>
            </Footer>
        </Container>
    </Fragment>
);

const Footer = styled.footer`
    padding-top: 10px;
    margin: 20px 0 -10px;
    border-top: 1.5px solid ${({ theme }) => (
        theme.colors.input.default
    )};
`;

const Icon = styled(Bitbucket)`
    width: 15px;
    margin-right: 5px;
`;