import React from 'react';
import { AngleLeft } from 'styled-icons/fa-solid';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { Heading, Paragraph, Mixins, Button } from '@cheapreats/react-ui';
import Layout from './Layout';

const IMAGE = 'https://steamuserimages-a.akamaihd.net/ugc/964222183902613085/669E0E9043DBFD12DE6158E60D5F43E5A7799E56/';

const _NotFound = ({ history }) => (
    <Layout>
        <Heading bold>Sorry, seems like that page dropped the course...</Heading>
        <Subtext bold>Error 404 - Page not found</Subtext>
        <Button margin='10px 0 0' onClick={ () => history.push('/') } icon={ AngleLeft }>
            Back to Home
        </Button>
        <Wrapper>
            <Image src={ IMAGE }/>
        </Wrapper>
    </Layout>
);

export const NotFound = withRouter(_NotFound);

NotFound.routerProps = {
    path: '/',
};

const Subtext = styled(Paragraph)`
    font-size: 1.4rem;
    margin-top: -5px;
    opacity: 0.6;
`;

const Wrapper = styled.div`
    ${Mixins.flex('center')}
    margin: 40px 0;
`;

const Image = styled.img`
    width: 70%;
`;