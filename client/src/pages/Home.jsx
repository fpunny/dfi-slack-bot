import React, { useState, useEffect, useCallback } from 'react';
import { Input, Heading, SmallText, Paragraph, Button } from '@cheapreats/react-ui';
import { PaperPlane } from 'styled-icons/fa-solid';
import { Results } from '../components';
import Layout from './Layout';

const _send = async (text, sq, ss) => {
    ss(true);
    try {
        const res = await fetch(process.env.REACT_APP_API, {
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            body: JSON.stringify({
                text
            })
        });
        const data = await res.json();

        if (data.error) throw Error(data.error);
        alert('Question Sent! (Refresh to see on list cus im too lazy to implement)');
        sq('');
    } catch (e) {
        console.error(e);
        alert('Unable to send question (RIP)');
    }
    ss(false);
}

export const Home = () => {
    const [search, setSearch] = useState('');
    const [question, setQuestion] = useState('');
    const [sending, setSending] = useState(false);

    useEffect(() => {
        const query = window.location.search;
        if (query.startsWith('?search=')) {
            setSearch(query.replace('?search=', ''));
        }
    }, []);

    const send = useCallback(() => (
        sending || _send(question, setQuestion, setSending)
    ), [question, sending]);

    return (
        <Layout>
            <Heading bold margin='0'>DFI Search thingy</Heading>
            <SmallText bold margin='-5px 0 15px' type='p'>Made in a few hours by yours truely</SmallText>
            <Input
                label='Search'
                placeholder='Look up questions'
                description='Searches question & answer for string in search'
                onChange={ ({ target }) => setSearch(target.value) }
                value={ search }
            />
            <Results search={ search }/>
            <Input
                label='Ask a Question'
                margin='40px 0 0'
                placeholder='When all hope is lost, ask the question'
                description='Make sure you search first...'
                onChange={ ({ target }) => setQuestion(target.value) }
                error={(
                    question.length !== 0 &&
                    question.length < 10 &&
                    'Must be atleast 10 characters because I highly doubt you have a question shorter than that'
                )}
                success={ question.length >= 10 }
                value={ question }
            />
            <Button
                disabled={question.length < 10}
                onClick={ send }
                icon={ PaperPlane }
                loading={ sending }
            >
                Send to DFI
            </Button>
            <Paragraph bold margin='20px 0 0' type='h2'>
                Need Help? Click <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://bitbucket.org/fpunny/dfi-slack-bot/src/master/'
                >
                    here
                </a>.
            </Paragraph>
        </Layout>
    );
};

Home.routerProps = {
    path: '/',
    exact: true,
};