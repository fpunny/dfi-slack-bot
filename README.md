# DFI Slack Bot

## Rules
 - Please be mature and don't DDOS, I swear to god I would cut off all additional communications with DFI except for in person.
 - Search for the question before asking, we don't want duplicates as it only makes everyone's life harder when looking up answers.
 - For any issues, you can contact me at [frederic.pun@mail.utoronto.ca](mailto:frederic.pun@mail.utoronto.ca) (Usually up until 3am)

## URLS
 - **Front-end: ** [https://dfi.fpunny.xyz/](https://dfi.fpunny.xyz/)
 - **Backend: ** [https://dfi-api.fpunny.xyz/](https://dfi-api.fpunny.xyz/)

## Endpoints
| Endpoint | Description |
| --- | --- |
| ```/questions [GET]``` | Returns all the questions |
| ```/questions?query=xxxx [GET]``` | Returns all the questions that contains the query as a substring in the question or answer |
| ```/questions/:id [GET]``` | Returns the question |
| ```/questions [POST]``` | Makes a question. I wouldn't go into details because you are unlikely to need this |

## FAQs
| Question | Answer |
| --- | --- |
| Why is the server so slow? | It's a free Heroku server, why would I buy a server for this? |
| Can I yoink this code? | Sure, why not. Keep it as a heirloom if you want |
| This code sucks | It was made in 8 hours, which includes me learning how to make the bot. So that's most likely why |
| Can I have feature x? | Feel free to make a PR for that. If I feel nice/free, I might make it (You can make it yourself too, I'll review it / merge it) |
| wHy Is A fRoNt-EnD dEv UsInG a Ui LiBrArY? | I made this UI library, everything from components, mixins, animations, to linting/compiling of library |
| Can I use the UI library? | Sure, but its under development. Here's what we have done so far, [link to thingy](https://cheapreats-ui.netlify.com) |
