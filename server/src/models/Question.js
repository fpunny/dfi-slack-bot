import mongoose from 'mongoose';

const Schema = new mongoose.Schema({
    text: {
        type: String,
        required: true,
    },
    answer: {
        type: String,
        default: null,
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    answered_at: {
        type: Date,
        default: null
    },
});

export const Question = mongoose.model('Question', Schema);