import { Question } from "./models";
import fetch from 'node-fetch';

// Thingy to send stuff to slack
const createPost = (channel, ts) => async text => {
    const res = await fetch('https://slack.com/api/chat.postMessage', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ process.env.SLACK_TOKEN }`
        },
        body: JSON.stringify({
            thread_ts: ts,
            channel,
            text,
        }),
    });
    const data = await res.json();
    if (!data.ok) console.error(data);
}

export default async (req, res, next) => {
    const { challenge, event } = req.body;
    const post = createPost(event.channel, event.ts);

    // For validation
    if (challenge) {
        res.send(challenge);
        return;
    }

    // Check input
    const id = event.text.match(/> (.*)\n/);
    const text = event.text.match(/```\n(.*)\n```/);
    if (!id || !text) {
        await post('The message is in the incorrect form');
        next(); return;
    }

    // Find question
    const question = await Question.findById(id[1]);
    if (!question) {
        await post('This question does not exist');
        next(); return;
    }

    // Check Question state
    if (question.answered_at) {
        await post('This question has already been answered');
        next(); return;
    }

    // Update question
    try {
        question.answered_at = new Date();
        question.answer = text[1];
        await question.save();
    } catch (e) {
        console.error(e);
        await post('Unable to save answer. Try again later');
        next(); return;
    }

    // Respond to DFI
    await post('Answer has been sent!');
    next();
};