import fetch from 'node-fetch';
import express from 'express';
import moment from 'moment';

import { Question } from '../models';
const router = express.Router();

const processMessage = ({ _id, created_at, text }) => (
`*ID: * ${ _id }
*Time: * ${ moment(created_at).format('LLLL') }
*Question: *
>>>${ text }
`
);

router.route('/')
    .post(async (req, res) => {

        // Check query
        const { text } = req.body;
        if (!text) {
            res.status(400).json({ error: 'Invalid query' });
            return;
        }

        // Make question in DB
        const question = new Question({ text });
        try {
            await question.save();
        } catch {
            res.status(400).json({ error: 'Unable to save question' });
            return;
        }

        // Make question for DFI
        try {
            await fetch(process.env.SLACK_POST, {
                method: 'POST',
                header: { 'Content-type': 'application/json' },
                body: JSON.stringify({ text: processMessage(question) })
            });
            res.json({ text: 'Sent' });
        } catch (e) {
            res.status(400).json({ error: 'Unable to send data' });
            console.error(e);
            await question.remove();
        }
    })
    .get(async (req, res) => {
        const { search } = req.query;

        // If has search string, find matching
        if (search) {
            const exp = new RegExp(search, 'i');
            res.json(await Question.find().or([
                { "text": exp }, { "answer": exp },
            ]));

        // Give me everything
        } else {
            res.json(await Question.find());
        }
    })
;

// Get me specific question
router.route('/:id')
    .get(async (req, res) => (
        res.json(await Question.findById(req.params.id))
    ))
;

export const slack = ['/questions', router];