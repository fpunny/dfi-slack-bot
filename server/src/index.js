import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';

// Initialization
dotenv.config();
const app = express();
mongoose.connect(process.env.DB, {
    useNewUrlParser: true
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// Setup routes
import * as routes from './routes';
Object.values(routes).forEach(route => app.use(...route));

// Default route
import handler from './handler';
app.route('/')
    .get((req, res) => res.send('Boo!'))
    .post(handler, (req, res) => res.send('DONE'))
;

// Sanity check
app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${ process.env.PORT }!`);
});